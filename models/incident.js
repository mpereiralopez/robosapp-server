'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var incidentDefinition = {
    deviceId: { type: String },
    latitude: { type: Number },
    longitude: { type: Number },
    country: { type: String },//FORMATO 2 CARACTERES
    incidentDate: { type: Number },//Guardar el long
    incidentTitle: { type: String },
    incidentDescription: { type: String },
    incidentCost: { type: Number },
    genre: { type: Boolean },
    incidentDenounced: { type: Boolean },
    email: { type: String },
    currency: { type: String, enum: ['USD', 'EUR', 'JPY', 'GBP', 'CHF', 'CAD', 'NZD', 'ZAR', 'MXN'], default: 'EUR' },
    stolenItem: {
        key : { type: String },
        user_description : { type: String }
    },
    incidentType: { type: Object }

}



var incidentSchema = new Schema(incidentDefinition);

module.exports = mongoose.model('Incident', incidentSchema);