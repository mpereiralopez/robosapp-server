'use strict';

var fs        = require('fs');
var path      = require('path');

var normalizedPath = path.join(__dirname, "");

fs.readdirSync(normalizedPath).forEach(function(file) {
  require(normalizedPath+'/'+file);
});