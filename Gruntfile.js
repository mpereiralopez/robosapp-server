var configFile =require('./config.json');

module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			options: { spawn: false },
			files: ['controllers/**', 'models/**','routes/**'],
			tasks: ['express:dev']
		},
		express: {
			options: {
				// Override defaults here
			},
			dev: {
				options: {
					script: './server.js',
					nospawn: true,
					delay: 10
				}
			},
			prod: {
				options: {
					script: 'server.js',
					node_env: 'production'
				}
			},
			test: {
				options: {
					script: 'cleqserver.js'
				}
			}
		},


		external_daemon: {
			mongodb: {
				cmd: 'mongod',
				args: ['--auth','--dbpath' ,configFile.dbOptions.dbpath],
				verbose: true
			}
		}

	});

	grunt.loadNpmTasks('grunt-express-server');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-external-daemon');


	grunt.registerTask('server_dev', ['external_daemon:mongodb', 'express:dev', 'watch']);

};
