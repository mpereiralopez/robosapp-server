var mongoose = require('mongoose');
var Incident = mongoose.model('Incident');

exports.findAllIncidentsByCountryCode = function (req, res) {
    var ISO2CountryCode = req.params.country;

    if (!ISO2CountryCode) {
        res.status(500).send("No estan todos los parametros");
        return;
    }
    console.log('ISO2CountryCode ' + ISO2CountryCode);
    Incident.find().
        where('country').equals(ISO2CountryCode).
        limit(500).
        exec(function (err, incidents) {
            if (err) res.status(500).send(err.message);

            console.log('GET /incidents_in_country/:country')
            res.status(200).jsonp(incidents);
        });

};


//GET - Return all incidents in the DB inside an area --> Para buscar por ciudad hay que usar solamente api google
exports.findAllIncidentsInArea = function (req, res) {
    console.log(req.params);
    console.log(req.query)
    var latSouthest = req.query.latS;
    var lonSouthest = req.query.lonS;
    var latNorthest = req.query.latN;
    var lonNorthest = req.query.lonN;
    console.log("latSouthest " + latSouthest);
    console.log("lonSouthest " + lonSouthest);
    console.log("latNorthest " + latNorthest);
    console.log("lonNorthest " + lonNorthest);
    if (!latSouthest || !lonSouthest || !latNorthest || !lonNorthest) {
        res.status(500).send("No estan todos los parametros");
        return;
    }

    /**
     * 
     * // Using query builder
Person.
  find({ occupation: /host/ }).
  where('name.last').equals('Ghost').
  where('age').gt(17).lt(66).
  where('likes').in(['vaporizing', 'talking']).
  limit(10).
  sort('-occupation').
  select('name occupation').
  exec(callback);
     * 
     */

    Incident.find().
        where('latitude').gt(latSouthest).lt(latNorthest).
        where('longitude').gt(lonNorthest).lt(lonSouthest).
        limit(500).
        exec(function (err, incidents) {
            if (err) res.status(500).send(err.message);

            console.log('GET /incidents_in_area/:lat/:lon')
            res.status(200).jsonp(incidents);
        });

};

//GET - Return all incidents in the DB
exports.findAllIncidents = function (req, res) {
    Incident.find(function (err, incidents) {
        if (err) res.status(500).send(err.message);

        console.log('GET /incidents')
        res.status(200).jsonp(incidents);
    });
};

//GET - Return a TVShow with specified ID
exports.findById = function (req, res) {
    Incident.findById(req.params.id, function (err, incident) {
        if (err) return res.status(500).send(err.message);
        console.log('GET /incident/' + req.params.id);
        res.status(200).jsonp(incident);
    });
};

//POST - Insert a new TVShow in the DB
exports.addIncident = function (req, res) {
    console.log('POST');
    console.log(req.body);



    var itemInfoToStore = {
        key: req.body.item,
        user_description: req.body.itemDescription
    }

    var incident = new Incident({
        deviceId: req.body.deviceId,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        country: req.body.country,
        incidentDate: req.body.incidentDate,
        incidentTitle: req.body.incidentTitle,
        incidentDescription: req.body.incidentDescription,
        incidentCost: req.body.incidentCost,
        genre: req.body.genre,
        incidentDenounced: req.body.incidentDenounced,
        email: req.body.email,
        currency: req.body.currency,
        stolenItem: itemInfoToStore,
        incidentType: req.body.incidentType
    });

    var promise = incident.save()

    promise.then(function (incident) {
        res.status(200).jsonp(incident);
    }).catch(function (err) {
        // just need one of these
        console.log('error:', err);
        res.status(500).send(err.message)
    });


};


exports.findAllIncidentsByItemType = function (req, res) {
    var item_key = req.params.item;
    console.log("item_key "+item_key);
    Incident.find({ 'stolenItem.key': item_key }, function (err, incidents) {
        if (err)  res.status(500).send(err.message);
        if (incidents) {
            console.log('GET /incidents_by_item/:item')
            res.status(200).jsonp(incidents);
        }
    });
};

exports.findAllIncidentsByGenre = function (req, res) {
    var isMale = req.params.isMale;
    console.log("isMale "+isMale);
    Incident.find({ 'genre': isMale }, function (err, incidents) {
        if (err)  res.status(500).send(err.message);
        if (incidents) {
            console.log('GET /incidents_by_genre/:isMale')
            res.status(200).jsonp(incidents);
        }
    });
};

exports.findAllIncidentsByIncidentType = function (req, res) {
    var type = req.params.type;
    console.log("item_key "+type);
    Incident.find({ 'incidentType': type }, function (err, incidents) {
        if (err)  res.status(500).send(err.message);
        if (incidents) {
            console.log('GET /incidents_by_type/:type')
            res.status(200).jsonp(incidents);
        }
    });
};