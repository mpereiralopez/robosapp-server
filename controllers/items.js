var itemDictionary = require('./../models/items.json');


exports.findAllItems = function (req, res) {
    console.log('GET /item_list/');
    res.status(200).jsonp(itemDictionary);
};