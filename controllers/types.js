var typesDictionary = require('./../models/incident_types.json');


exports.findAllTypes = function (req, res) {
    console.log('GET /type_list/');
    res.status(200).jsonp(typesDictionary);
};