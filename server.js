var express = require("express"),  
    server = express(),
    bodyParser  = require("body-parser"),
    methodOverride = require("method-override"),
    db = require('./models');
    mongoose = require('mongoose'),
    config = require('./config.json'),
    errors = require('./errors.json');



mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/incidents',config.dbOptions, function(err, res) {  
  if(err) {
    console.log('ERROR: connecting to Database. ' + err);
  }
});


server.use(bodyParser.urlencoded({ extended: false }));  
server.use(bodyParser.json());  
server.use(methodOverride());



// a middleware with no mount path; gets executed for every request to the app
server.use(function(req, res, next) {
  var providerId = req.headers.provider_identifier;
  console.log(providerId)
  if(providerId == config.provider_identifier){
    next();
  }else{
      res.status(errors.provider_error.status).send(errors.provider_error.msg)
  }
  
});


require('./routes/routes')(server);




 server.listen(3000, function() {
    console.log("Node server running on http://localhost:3000");
  });