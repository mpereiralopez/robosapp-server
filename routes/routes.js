'use strict';

var path = require('path');
//auth = require('../config/auth');

var express = require('express');
var router = express.Router();
//var passport = require('passport');
var incident = require('../controllers/incidents');
var item = require('../controllers/items');
var types = require('../controllers/types');


module.exports = function (router) {


    // Routes for incidents*/
    router.get('/incident/:id', incident.findById);
    router.get('/incidents', incident.findAllIncidents);
    router.get('/incidents_in_area', incident.findAllIncidentsInArea);
    router.post('/incident', incident.addIncident);
    router.get('/incidents_in_country/:country', incident.findAllIncidentsByCountryCode);

    router.get('/incidents_by_item/:item', incident.findAllIncidentsByItemType);
    router.get('/incidents_by_type/:type', incident.findAllIncidentsByIncidentType);

    router.get('/incidents_by_genre/:isMale', incident.findAllIncidentsByGenre);


    router.get('/item_list', item.findAllItems);
    router.get('/incident_types_list', types.findAllTypes);


    /*
     // User Routes
     router.post('/auth/users', users.create);
     router.get('/auth/check/',users.exists);
    
    
    
    
      // Note Routes
      router.get('/api/notes', notes.all);
      router.post('/api/notes', auth.ensureAuthenticated, notes.create);
    
      router.route('/api/notes/:noteId')
      .get(function(req,res){
    
        auth.ensureAuthenticated(req,res,function(){
          auth.hasAuthorization(req,res,function(){
            notes.getNote(req.params.noteId).then(function(note){
              res.status(200).json(note);
            });
          })
        })
      })
      .delete(function (req, res) {
        notes.destroy(req.params.noteId).then(function(note){
          res.status(200).json(note);
        });
      });
    
    
      router.get('/partials/*', function(req, res) {
        var requestedView = path.join('./', req.url);
        console.log(requestedView);
        res.render(requestedView);
      });
    
      router.get('/*', function(req, res) {
        res.render('index');
      });*/

      

}